/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.initialconditions;

import java.util.ArrayList;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.engine.SycamoreRobot;
import it.diunipi.volpi.sycamore.engine.SycamoreRobotMatrix;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditionsImpl;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 * @author harry
 *
 */
@PluginImplementation
public class InitialConditionOnVertices extends InitialConditionsImpl<Point2D> {

	private String	_author				= "Harish Prakash";
	private TYPE	_type				= TYPE.TYPE_2D;
	private String	_shortDescription	= "Robots on Vertices";
	private String	_pluginName			= "RobotsOnVertices";

	// @formatter:off
	private String	_description	= "Position robots on regular polygon vertices. "+
									  "Enter the number of vertices in the \"Number of Vertices\" field, "+
									  "the distance between robots in the \"Distance\" field, "+
									  "the degrees you wish to rotate the polygon\'s vertex in the \"Rotate\" field, "+
									  "the center of the polygon in the \"Center\" field. "+
									  "The default values for these fields are "+
									  "3 vertices "+
									  "seperated by the distance selected in visibility configuration panel "+
									  "rotated by 0 degrees "+
									  "and centered at (0, 0)";
	// @formatter:on

	// Generic Inherited properties
	@Override
	public TYPE getType() {

		return _type;
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return InitialConditionOnVerticesConfigurationPanel.getUniqueInstance();
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}
	// END OF Generic Inherited Properties

	@Override
	public Point2D nextStartingPoint(SycamoreRobotMatrix<Point2D> robots) {

		InitialConditionOnVerticesConfigurationPanel config = InitialConditionOnVerticesConfigurationPanel.getUniqueInstance();

		// Collate all robot positions
		java.util.Iterator<SycamoreRobot<Point2D>> robotIterator = robots.iterator();
		ArrayList<Point2D> robotPositions = new ArrayList<>();
		Point2D[] array_robotPositions;
		while (robotIterator.hasNext()) {
			robotPositions.add(robotIterator.next().getGlobalPosition());
		}
		array_robotPositions = new Point2D[robotPositions.size()];
		array_robotPositions = robotPositions.toArray(array_robotPositions);

		// Rotate a 90 degrees for uniformity
		double current = (config.getInternalAngle() * (robots.robotsCount() % config.getNumberOfVertices())) + (config.getRotateAngle() + 90);
		Point2D center = config.getCenter(array_robotPositions);

		float x = (float) (center.x + Math.cos(Math.toRadians(current)) * config.getPolarDistance());
		float y = (float) (center.y + Math.sin(Math.toRadians(current)) * config.getPolarDistance());
		Point2D point = new Point2D(x, y);

		return point;
	}

}
