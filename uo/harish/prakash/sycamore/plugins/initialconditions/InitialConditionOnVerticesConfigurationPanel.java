/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.initialconditions;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.gui.SycamoreSystem;
import it.diunipi.volpi.sycamore.plugins.visibilities.VisibilityImpl;

/**
 * @author harry
 *
 */
public class InitialConditionOnVerticesConfigurationPanel extends SycamorePanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static InitialConditionOnVerticesConfigurationPanel uniqueInstance;

	private JLabel		labelNumberOfVertices;
	private JLabel		labelRotate;
	private JLabel		labelDistance;
	private JLabel		labelPolarDistance;
	private JLabel		labelCenter;
	private JLabel		labelCoordinateX;
	private JLabel		labelCoordinateY;
	private JTextField	textNumberOfVertices;
	private JTextField	textRotate;
	private JTextField	textDistance;
	private JTextField	textPolarDistance;
	private JTextField	textCoordinateX;
	private JTextField	textCoordinateY;
	private JPanel		panelContent;

	@SuppressWarnings("rawtypes")
	@Override
	public void setAppEngine(SycamoreEngine appEngine) {}

	@Override
	public void updateGui() {}

	@Override
	public void reset() {}

	private InitialConditionOnVerticesConfigurationPanel() {
		setupGUI();
	}

	private void setupGUI() {

		BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		setLayout(layout);

		GridBagLayout contentLayout = new GridBagLayout();
		contentLayout.columnWeights = new double[] { Double.MIN_VALUE, Double.MIN_VALUE, 0.5, Double.MIN_VALUE, 0.5 };
		contentLayout.rowWeights = new double[] { Double.MIN_NORMAL, Double.MIN_NORMAL, Double.MIN_NORMAL, Double.MIN_NORMAL, 1 };
		getPanelContent().setLayout(contentLayout);

		GridBagConstraints c;

		// Create Labels
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.insets = new Insets(3, 2, 3, 2);
		getPanelContent().add(getLabelNumberOfVertices(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.insets = new Insets(3, 2, 3, 2);
		getPanelContent().add(getLabelDistance(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 2;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.insets = new Insets(3, 2, 3, 2);
		getPanelContent().add(getLabelPolarDistance(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 3;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.insets = new Insets(3, 2, 3, 2);
		getPanelContent().add(getLabelRotate(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 4;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.insets = new Insets(3, 2, 3, 2);
		getPanelContent().add(getLabelCenter(), c);

		// Create Labels for coordinates
		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 4;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.insets = new Insets(3, 0, 3, 0);
		getPanelContent().add(getLabelCoordinateX(), c);

		c = new GridBagConstraints();
		c.gridx = 3;
		c.gridy = 4;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.insets = new Insets(3, 0, 3, 0);
		getPanelContent().add(getLabelCoordinateY(), c);

		// Create TextFields
		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 4;
		c.insets = new Insets(3, 2, 3, 2);
		getPanelContent().add(getTextNumberOfVertices(), c);

		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 1;
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 4;
		c.insets = new Insets(3, 2, 3, 2);
		getPanelContent().add(getTextDistance(), c);

		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 2;
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 4;
		c.insets = new Insets(3, 2, 3, 2);
		getPanelContent().add(getTextPolarDistance(), c);

		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 3;
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 4;
		c.insets = new Insets(3, 2, 3, 2);
		getPanelContent().add(getTextRotate(), c);

		// Create TextFields for coordinates
		c = new GridBagConstraints();
		c.gridx = 2;
		c.gridy = 4;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(3, 5, 3, 2);
		getPanelContent().add(getTextCoordinateX(), c);

		c = new GridBagConstraints();
		c.gridx = 4;
		c.gridy = 4;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(3, 5, 3, 2);
		getPanelContent().add(getTextCoordinateY(), c);

		// Setup JPanel
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 5;
		getPanelContent().add(new JPanel(), c);

		// Complete setup
		add(getPanelContent());
		setBorder(BorderFactory.createTitledBorder("Vertice configuration"));
		setPreferredSize(new Dimension(500, getPreferredSize().height));
	}

	/**
	 * @return the uniqueInstance
	 */
	public static InitialConditionOnVerticesConfigurationPanel getUniqueInstance() {

		if (uniqueInstance == null) {
			uniqueInstance = new InitialConditionOnVerticesConfigurationPanel();
		}
		return uniqueInstance;
	}

	/**
	 * @return the labelNumberOfVertices
	 */
	private JLabel getLabelNumberOfVertices() {

		if (labelNumberOfVertices == null) {
			labelNumberOfVertices = new JLabel("Number of vertices");
		}
		return labelNumberOfVertices;
	}

	/**
	 * @return the labelRadius
	 */
	private JLabel getLabelDistance() {

		if (labelDistance == null) {
			labelDistance = new JLabel("Distance");
		}

		return labelDistance;
	}

	/**
	 * @return the labelPolarDistance
	 */
	private JLabel getLabelPolarDistance() {

		if (labelPolarDistance == null) {
			labelPolarDistance = new JLabel("Polar Distance");
		}
		return labelPolarDistance;
	}

	/**
	 * @return the labelRotate
	 */
	private JLabel getLabelRotate() {

		if (labelRotate == null) {
			labelRotate = new JLabel("Rotate");
		}
		return labelRotate;
	}

	/**
	 * @return the labelCenter
	 */
	private JLabel getLabelCenter() {

		if (labelCenter == null) {
			labelCenter = new JLabel("Center");
		}
		return labelCenter;
	}

	/**
	 * @return the labelCoordinateX
	 */
	private JLabel getLabelCoordinateX() {

		if (labelCoordinateX == null) {
			labelCoordinateX = new JLabel("x");
		}
		return labelCoordinateX;
	}

	/**
	 * @return the labelCoordinateY
	 */
	private JLabel getLabelCoordinateY() {

		if (labelCoordinateY == null) {
			labelCoordinateY = new JLabel("y");
		}
		return labelCoordinateY;
	}

	/**
	 * @return the textNumberOfVertices
	 */
	private JTextField getTextNumberOfVertices() {

		if (textNumberOfVertices == null) {
			textNumberOfVertices = new JTextField();
		}
		return textNumberOfVertices;
	}

	/**
	 * @return the textRadius
	 */
	private JTextField getTextDistance() {

		if (textDistance == null) {
			textDistance = new JTextField();
		}

		return textDistance;
	}

	/**
	 * @return the textPolarDistance
	 */
	private JTextField getTextPolarDistance() {

		if (textPolarDistance == null) {
			textPolarDistance = new JTextField();
		}
		return textPolarDistance;
	}

	/**
	 * @return the textRotate
	 */
	private JTextField getTextRotate() {

		if (textRotate == null) {
			textRotate = new JTextField();
		}
		return textRotate;
	}

	/**
	 * @return the textCoordinateX
	 */
	private JTextField getTextCoordinateX() {

		if (textCoordinateX == null) {
			textCoordinateX = new JTextField();
		}
		return textCoordinateX;
	}

	/**
	 * @return the textCoordinateY
	 */
	private JTextField getTextCoordinateY() {

		if (textCoordinateY == null) {
			textCoordinateY = new JTextField();
		}
		return textCoordinateY;
	}

	public int getNumberOfVertices() {

		try {
			int vertices = Integer.parseInt(getTextNumberOfVertices().getText());
			return (vertices < 3 ? 3 : vertices);
		}
		catch (NumberFormatException ex) {
			return 3;
		}
	}

	public double getInternalAngle() {

		return ((double) 360) / getNumberOfVertices();
	}

	public float getDistance() {

		try {
			return Float.parseFloat(getTextDistance().getText()) - SycamoreSystem.getEpsilon();
		}
		catch (NumberFormatException ex) {
			return VisibilityImpl.getVisibilityRange();
		}
	}

	public double getPolarDistance() {

		double polarDistance = 0;
		try {
			polarDistance = Double.parseDouble(getTextPolarDistance().getText());
		}
		catch (NumberFormatException | NullPointerException ex) {

			float angle = ((float) 360) / getNumberOfVertices();
			float alpha = (180 - angle) / 2;
			polarDistance = (Math.sin(Math.toRadians(alpha)) / Math.sin(Math.toRadians(angle))) * getDistance();
		}

		return polarDistance;
	}

	public float getRotateAngle() {

		try {
			return Float.parseFloat(getTextRotate().getText());
		}
		catch (NumberFormatException ex) {
			return 0;
		}
	}

	public Point2D getCenter(Point2D[] robotPositions) {

		try {
			Point2D center = new Point2D(Float.parseFloat(getTextCoordinateX().getText()), Float.parseFloat(
				getTextCoordinateY().getText()));
			return center;
		}
		catch (NumberFormatException ex) {

			return uo.harish.prakash.math.SmallestCircle.of(robotPositions).getCenter();
		}
	}

	/**
	 * @return the panelContent
	 */
	private JPanel getPanelContent() {

		if (panelContent == null) {
			panelContent = new JPanel();
		}
		return panelContent;
	}

}
